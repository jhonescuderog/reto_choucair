# Autor: Jhon Escudero
@stories
Feature: Utest
  Como usuario, automatizare con screamplay el registro en la plataforma utest.
  @scenario1
  Scenario: Registrando un nuevo usuario
    Given que Jhon quiere registrarse
    When esta diligenciando el formulario de registro
      |nombre       |apellido          |email                       |mes    |dia    |anio   |ciudad                                    |postal   |contrasenia      |
      |Jhon Wilder  |Escudero Galeano  |jhon-escuderOG@hotmail.com   |June   |21     |1994   |San Carlos San Carlos Antioquia, Colombia |054420   |Guadalupe59780*   |
    Then verifica el registro
    |mensajeRegistro                                                            |
    |Welcome to the world's largest community of freelance software testers!    |