package co.com.choucair.certification.reto_jhonescudero.stepdefinitions;

import co.com.choucair.certification.reto_jhonescudero.model.UtestData;
import co.com.choucair.certification.reto_jhonescudero.questions.Answer;
import co.com.choucair.certification.reto_jhonescudero.tasks.AbrirNavegador;


import co.com.choucair.certification.reto_jhonescudero.tasks.Ingresar;
import co.com.choucair.certification.reto_jhonescudero.tasks.Registro;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


import java.util.List;

public class UtestStepDefinitions {

    @Before
    public void setStage (){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que Jhon quiere registrarse$")
    public void queJhonQuiereRegistrarse() {
        OnStage.theActorCalled("Jhon").wasAbleTo(AbrirNavegador.thePage(),
                Ingresar.onThePage());
    }

    @When("^esta diligenciando el formulario de registro$")
    public void estaDiligenciandoElFormularioDeRegistro(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Registro.the(utestData.get(0).getNombre()
                ,utestData.get(0).getApellido()
                ,utestData.get(0).getEmail()
                ,utestData.get(0).getMes()
                ,utestData.get(0).getDia()
                ,utestData.get(0).getAnio()
                ,utestData.get(0).getCiudad()
                ,utestData.get(0).getPostal()
                ,utestData.get(0).getContrasenia()));
    }

    @Then("^verifica el registro$")
    public void verificaElRegistro(List<UtestData> utestData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getMensajeRegistro())));
    }


}
