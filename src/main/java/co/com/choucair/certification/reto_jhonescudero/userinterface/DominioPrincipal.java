package co.com.choucair.certification.reto_jhonescudero.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class DominioPrincipal {

    public static final Target BOTON_REGISTRO= Target.the("Boton para ingresar al registro")
            .located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a"));


}
