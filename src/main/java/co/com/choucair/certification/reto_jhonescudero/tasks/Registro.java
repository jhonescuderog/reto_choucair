package co.com.choucair.certification.reto_jhonescudero.tasks;

import co.com.choucair.certification.reto_jhonescudero.userinterface.PaginaRegistroCuatro;
import co.com.choucair.certification.reto_jhonescudero.userinterface.PaginaRegistroDos;
import co.com.choucair.certification.reto_jhonescudero.userinterface.PaginaRegistroTres;
import co.com.choucair.certification.reto_jhonescudero.userinterface.PaginaRegistroUno;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.RecordsInputs;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hover;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.conditions.Check;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class Registro implements Task {

    private String nombre;
    private String apellido;
    private String email;
    private String mes;
    private String dia;
    private String anio;

    private String ciudad;
    private String postal;

    private String contrasenia;

    public Registro(String nombre, String apellido, String email, String mes, String dia, String anio, String ciudad,String postal, String contrasenia) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.mes = mes;
        this.dia = dia;
        this.anio = anio;

        this.ciudad = ciudad;
        this.postal = postal;

        this.contrasenia = contrasenia;
    }


    public static Registro the(String nombre,String apellido,String email,String mes,String dia,String anio, String ciudad,String postal, String contrasenia) {
        return Tasks.instrumented(Registro.class,nombre,apellido,email,mes,dia,anio,ciudad,postal, contrasenia);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(nombre).into(PaginaRegistroUno.INGRESAR_NOMBRE),
                Enter.theValue(apellido).into(PaginaRegistroUno.INGRESAR_APELLIDO),
                Enter.theValue(email).into(PaginaRegistroUno.INGRESAR_EMAIL),
                SelectFromOptions.byVisibleText(mes).from(PaginaRegistroUno.INGRESAR_MES),
                SelectFromOptions.byVisibleText(dia).from(PaginaRegistroUno.INGRESAR_DIA),
                SelectFromOptions.byVisibleText(anio).from(PaginaRegistroUno.INGRESAR_ANIO),
                Click.on(PaginaRegistroUno.BOTON_NEXT),

                Enter.theValue(ciudad).into(PaginaRegistroDos.INGRESAR_CIUDAD),
                Enter.theValue(postal).into(PaginaRegistroDos.INGRESAR_POSTAL),
                Click.on(PaginaRegistroDos.INGRESAR_PAIS),
                WaitUntil.the(PaginaRegistroDos.OPCION_COLOMBIA, isVisible()).forNoMoreThan(10).seconds(),
                Click.on(PaginaRegistroDos.OPCION_COLOMBIA),

                Click.on(PaginaRegistroDos.BOTON_NEXT),

                Click.on(PaginaRegistroTres.MARCA_CELULAR),
                WaitUntil.the(PaginaRegistroTres.OPCION_APPLE, isVisible()).forNoMoreThan(10).seconds(),
                Click.on(PaginaRegistroTres.OPCION_APPLE),
                Click.on(PaginaRegistroTres.MODELO_CELULAR),
                WaitUntil.the(PaginaRegistroTres.OPCION_IPHONE_12_PRO_MAX, isVisible()).forNoMoreThan(10).seconds(),
                Click.on(PaginaRegistroTres.OPCION_IPHONE_12_PRO_MAX),
                Click.on(PaginaRegistroTres.SISTEMA_CELULAR),
                WaitUntil.the(PaginaRegistroTres.SISTEMA_CELULAR_IOS, isVisible()).forNoMoreThan(10).seconds(),
                Click.on(PaginaRegistroTres.SISTEMA_CELULAR_IOS),
                Click.on(PaginaRegistroTres.BOTON_NEXT),

                Enter.theValue(contrasenia).into(PaginaRegistroCuatro.INGRESAR_CONTRASENIA),
                Enter.theValue(contrasenia).into(PaginaRegistroCuatro.CONFIRMAR_CONTRASENIA),
                Click.on(PaginaRegistroCuatro.CHECKBOX_TERMINOS),
                Click.on(PaginaRegistroCuatro.CHECKBOX_PRIVACIDAD),
                Click.on(PaginaRegistroCuatro.BOTON_NEXT)
        );
    }
}
//Click.on(PaginaRegistroDos.INGRESAR_CIUDAD),