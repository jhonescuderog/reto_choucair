package co.com.choucair.certification.reto_jhonescudero.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaRegistroUno {

    public static final Target INGRESAR_NOMBRE= Target.the("Ingresa nombre")
            .located(By.id("firstName"));
    public static final Target INGRESAR_APELLIDO= Target.the("Ingresa apellido")
            .located(By.id("lastName"));
    public static final Target INGRESAR_EMAIL= Target.the("Ingresa email")
            .located(By.id("email"));
    public static final Target INGRESAR_MES= Target.the("Ingresa mes de nacimiento")
            .located(By.id("birthMonth"));
    public static final Target INGRESAR_DIA= Target.the("Ingresa dia de nacimiento")
            .located(By.id("birthDay"));
    public static final Target INGRESAR_ANIO= Target.the("Ingresa año de nacimiento")
            .located(By.id("birthYear"));
    public static final Target BOTON_NEXT= Target.the("Boton siguiente")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/a"));



}
