package co.com.choucair.certification.reto_jhonescudero.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaRegistroDos {

    public static final Target INGRESAR_CIUDAD= Target.the("Ingresa ciudad")
            .located(By.id("city"));

    public static final Target INGRESAR_POSTAL= Target.the("Ingresa codigo postal")
            .located(By.id("zip"));
    public static final Target INGRESAR_PAIS= Target.the("Ingresa pais")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div"));

    public static final Target OPCION_COLOMBIA= Target.the("Pais Colombia")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/ul/li/div[49]/span/div"));

    public static final Target BOTON_NEXT= Target.the("Boton siguiente")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/div/a"));
}
