package co.com.choucair.certification.reto_jhonescudero.questions;

import co.com.choucair.certification.reto_jhonescudero.userinterface.RegistroExitoso;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String question;

    public Answer(String question) {
        this.question = question;
    }

    public static Question<Boolean> toThe(String question) {
        return new Answer(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String registro_completo= Text.of(RegistroExitoso.ENCABEZADO).viewedBy(actor).asString();
        if(question.equals(registro_completo)){
            result=true;
        }else{
            result = false;
        }

        return result;
    }
    
}
