package co.com.choucair.certification.reto_jhonescudero.tasks;

import co.com.choucair.certification.reto_jhonescudero.userinterface.DominioPrincipal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Ingresar implements Task {

    public static Ingresar onThePage() {
        return Tasks.instrumented(Ingresar.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(DominioPrincipal.BOTON_REGISTRO)
        );
    }
}
