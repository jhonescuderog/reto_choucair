package co.com.choucair.certification.reto_jhonescudero.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaRegistroCuatro {

    public static final Target INGRESAR_CONTRASENIA= Target.the("Ingresar contraseña")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[3]/div[1]/input"));

    public static final Target CONFIRMAR_CONTRASENIA= Target.the("Confrimar contraseña")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[3]/div[2]/input"));

    public static final Target CHECKBOX_TERMINOS= Target.the("Check Terminos y condiciones")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[5]/label/input"));

    public static final Target CHECKBOX_PRIVACIDAD= Target.the("Check Politicas de privacidad")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[6]/label/input"));

    public static final Target BOTON_NEXT= Target.the("Boton completar registro")
            .located(By.id("laddaBtn"));
}