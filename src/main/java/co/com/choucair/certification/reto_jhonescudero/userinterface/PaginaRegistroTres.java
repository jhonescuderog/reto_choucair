package co.com.choucair.certification.reto_jhonescudero.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class PaginaRegistroTres {

        public static final Target MARCA_CELULAR= Target.the("Marca de celular")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[2]/div/div[1]/span/span[1]"));

    public static final Target OPCION_APPLE= Target.the("Marca Apple")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[2]/div/ul/li/div[5]/span/div"));

    public static final Target MODELO_CELULAR= Target.the("Modelo celular")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/div[1]/span"));

    public static final Target OPCION_IPHONE_12_PRO_MAX= Target.the("Modelo Iphone 12 pro max")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/ul/li/div[75]/span/div"));

    public static final Target SISTEMA_CELULAR= Target.the("Sistema Operativo")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[3]/div[2]/div/div[1]/span"));

    public static final Target SISTEMA_CELULAR_IOS= Target.the("Sistema iOS 14.7")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[3]/div[2]/div/ul/li/div[17]/span/div"));

    public static final Target BOTON_NEXT= Target.the("Boton siguiente")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[2]/div/a"));
}