package co.com.choucair.certification.reto_jhonescudero.tasks;

import co.com.choucair.certification.reto_jhonescudero.userinterface.PaginaUtest;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirNavegador implements Task {

    private PaginaUtest paginaUtest;

    public static AbrirNavegador thePage() {
        return Tasks.instrumented(AbrirNavegador.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(paginaUtest));
    }
}
